# Firefox extension for wallabag

## Contributing

### Tools

To contribute, you need [PureScript, Pulp and Bower](https://github.com/bodil/pulp#installation) to compile the PureScript to JavaScript. You also need [JPM](https://developer.mozilla.org/en-US/Add-ons/SDK/Tools/jpm) to build the extension into an XPI.

```bash
npm install -g purescript pulp bower jpm
```

### First installation

Install the dependencies.

```bash
bower install
```

### Workflow

Build the PureScript source files (from `src`) into one optimized (`-O` parameter) `index.js` file.

```bash
pulp build -O -t output/index.js
```

Pack the `index.js` file (and `data`, `locale`…) into an XPI extension file named `wallabag.xpi`.

```bash
jpm xpi
```

Run a fresh instance of Firefox. Logs will be printed in the console.

```bash
jpm run -b /usr/bin/firefox
```

Eventualy, visit `about:debugging` and click on "Load Temporary Add-on". Select your `wallabag.xpi` file.

### Source code

The extension is event-driven following the [Elm architecture](https://guide.elm-lang.org/architecture/). The state of the application is represented by the `State` type defined in `src/Data/State.purs`.
