module Notifications where

import Sdk.Notifications as Sdk
import Control.Monad (bind)
import Control.Monad.Eff (Eff)
import Data.String (joinWith)
import Prelude (Unit, ($), (<$>), (<*>), (>>=))
import Sdk.L10n (L10N, translate)

data Notification = UrlIsInvalid | UrlIsAlreadySaved

notify :: forall eff. Notification -> (Eff (notifications :: Sdk.NOTIFICATIONS, l10n :: L10N | eff) Unit)
notify UrlIsInvalid = notification "url_is_invalid" >>= Sdk.notify
notify UrlIsAlreadySaved = notification "url_is_invalid" >>= Sdk.notify

notification :: forall eff. String -> (Eff (l10n :: L10N | eff)) Sdk.Notification
notification name = { title: _, text: _, iconURL: "./images/icon.svg"}
  <$> (translate $ key "title")
  <*> (translate $ key "text")
  where
  key _type = joinWith "_" ["notifications", name, _type]
