module Gui where

import Sdk.Panel (Panel)
import Sdk.Ui.Button.Toggle (Button)

type Gui = { button :: Button
           , panel :: Panel
           }
