module Main where

import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (logShow)
import Control.Monad.Eff.Ref (readRef, modifyRef', Ref, newRef)
import Data.Button (create)
import Data.Config (Action, Event, fromPreferences, addEventListeners, update, action, effect) as Config
import Data.Entries (Event, init, addEventListenerForTabChange, update) as Entries
import Data.Gui (create, addEventListeners) as Gui
import Data.State (getEntries, getGui, State(State), Event(..), Action(..))
import Data.Traversable (sequence)
import Prelude (map, ($), (<$>), (<*>), (>>>), pure, unit, Unit, bind)
import Sdk.Panel (PANEL)
import Sdk.SimplePrefs (PREFERENCES)
import Sdk.Tabs (TABS)
import Sdk.Ui.Button.Toggle (BUTTON)

init :: forall eff. Eff (preferences :: PREFERENCES, tabs :: TABS, button :: BUTTON, panel :: PANEL | eff) State
init = map State $ { entries: _
                   , config: _
                   , gui: _
                   }
                   <$> Entries.init
                   <*> Config.fromPreferences
                   <*> Gui.create

main :: Eff _ Unit
main = do
  initialState <- init
  state <- newRef initialState
  let unsafeUpdate = unsafeUpdate' state

  Entries.addEventListenerForTabChange (EntriesEvent >>> unsafeUpdate)
  Config.addEventListeners (ConfigEvent >>> unsafeUpdate)
  Gui.addEventListeners (getGui initialState) (GuiEvent >>> unsafeUpdate) (ConfigEvent >>> unsafeUpdate)

unsafeUpdate' :: Ref State -> Event -> Eff _ Unit
unsafeUpdate' state event = do
  logShow event
  actions <- modifyRef' state (update event)
  sequence $ map effect actions
  newState <- readRef state
  logShow newState

update :: Event -> State -> { state :: State, value :: Array Action }
update (EntriesEvent event) (State state) = { state: State $ state {entries = Entries.update event state.entries}, value: []}
update (ConfigEvent event) (State state) = { state: State $ state { config = Config.update event state.config}, value: ConfigAction <$> Config.action event}
update (GuiEvent event) state = { state: state, value: []}

effect :: Action -> Eff _ Unit
effect (ConfigAction action) = Config.effect action
