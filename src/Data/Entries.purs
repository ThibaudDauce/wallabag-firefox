module Data.Entries where

import Control.Monad.Eff (Eff)
import Data.Foldable (foldMap)
import Data.List (concatMap)
import Data.Map (values, pop, insert, empty, Map)
import Data.Maybe (Maybe(Just, Nothing))
import Data.Remote (Remote(Local))
import Data.Synchronisable (Synchronisable(Nothing)) as S
import Data.Tuple (Tuple(Tuple))
import Data.Url (Url(InvalidUrl, Url), fromTab, parse) as Url
import Prelude (append, show, (<>), class Show, id, Unit, ($), (<$>), (>>>))
import Sdk.Tabs (onTabChange, TABS)

data Status = Saved | Unsaved
newtype Entry = Entry { status :: Remote Status
                      , url :: Url.Url
                      }

newtype Entries = Entries { current :: Entry
                          , others :: Map String Entry
                          }

data Event = UrlHasChanged Url.Url

createUnsavedEntry :: Url.Url -> Entry
createUnsavedEntry = Entry <$> {status: Local S.Nothing Unsaved, url: _}

getUrl :: Entry -> Url.Url
getUrl (Entry entry) = entry.url

instance showEvent :: Show Event where
  show (UrlHasChanged url) = "the URL has changed for " <> show url

instance showStatus :: Show Status where
  show Saved = "Saved"
  show Unsaved = "Unsaved"

instance showEntry :: Show Entry where
  show (Entry entry) = show entry.url <> " (" <> show entry.status <> ")"

instance showEntries :: Show Entries where
  show (Entries entries) = "Current entry: " <> show entries.current <> "\n"
                        <> "Other entries: \n"
                        <> foldMap (show >>> (append "\n")) (values entries.others)

init :: forall eff. Eff (tabs :: TABS | eff) Entries
init = Entries <$> ({ current: _, others: empty :: Map String Entry } <$> fromTab)

fromTab :: forall eff. Eff (tabs :: TABS | eff) Entry
fromTab = createUnsavedEntry <$> Url.fromTab

addEventListenerForTabChange :: forall a b. (Event -> (Eff a Unit)) -> Eff (tabs :: TABS | b) Unit
addEventListenerForTabChange sendEvent = onTabChange (Url.parse >>> UrlHasChanged >>> sendEvent)

update :: Event -> Entries -> Entries
update event entries = case event of
  UrlHasChanged url -> toggleUrl url entries

toggleUrl :: Url.Url -> Entries -> Entries
toggleUrl newUrl entries = case newUrl of
  Url.InvalidUrl -> saveCurrentUrlToOthers entries
  Url.Url newStringUrl -> (saveCurrentUrlToOthers >>> popFromOthersToCurrent newStringUrl) entries

saveCurrentUrlToOthers :: Entries -> Entries
saveCurrentUrlToOthers entries = saveEntryToOthers (getCurrent entries) entries

popFromOthersToCurrent :: String -> Entries -> Entries
popFromOthersToCurrent stringUrl (Entries entries) = Entries $ case pop stringUrl entries.others of
  Nothing -> entries { current = createUnsavedEntry (Url.Url stringUrl) }
  Just (Tuple saved otherEntries) -> entries { current = saved, others = otherEntries }

saveEntryToOthers :: Entry -> Entries -> Entries
saveEntryToOthers newEntry (Entries entries)  = Entries $ case getUrl newEntry of
    Url.InvalidUrl -> entries
    Url.Url oldStringUrl -> entries { others = insert oldStringUrl newEntry entries.others }

getCurrent :: Entries -> Entry
getCurrent (Entries entries) = entries.current
