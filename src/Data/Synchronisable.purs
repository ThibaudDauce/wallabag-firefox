module Data.Synchronisable where

import Prelude ((<>), show, class Show)

data Synchronisable a = Nothing | Sending a | Sent a | NeedAsking | Asked

instance showSynchronisable :: (Show a) => Show (Synchronisable a) where
  show Nothing = "no current sync action"
  show (Sending x) = "sending " <> show x
  show (Sent x) = "sent " <> show x
  show (NeedAsking) = "need to ask synchronisation"
  show (Asked) = "asked for synchronisation"
