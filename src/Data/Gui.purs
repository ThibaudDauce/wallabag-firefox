module Data.Gui where

import Control.Monad.Eff (Eff)
import Data.Button (Event(..), create) as B
import Prelude (Unit, ($), (<$>), (<*>), map, bind, (>>>))
import Sdk.Ui.Button.Toggle (BUTTON, Button, onButtonClick)
import Sdk.Panel (PANEL)
import Data.Panel (Panel, Event, create, addEventListeners) as P
import Data.Config (Config, Event) as Config

newtype Gui = Gui { button :: Button
                  , panel :: P.Panel
                  }

data Event = ButtonEvent B.Event | PanelEvent P.Event

create :: forall eff. (Eff (button :: BUTTON, panel :: PANEL | eff) Gui)
create = map Gui $ { button: _
                   , panel: _
                   }
                   <$> B.create
                   <*> P.create

addEventListeners :: forall a b. Gui -> (Event -> (Eff a Unit)) -> (Config.Event -> (Eff a Unit)) -> Eff (button :: BUTTON, panel :: PANEL | b) Unit
addEventListeners (Gui gui) sendGuiEvent sendConfigEvent = do
  onButtonClick gui.button (sendGuiEvent $ ButtonEvent B.ButtonWasClicked)
  P.addEventListeners gui.panel (PanelEvent >>> sendGuiEvent) sendConfigEvent

update :: Event -> Config.Config -> Gui -> Gui
update (ButtonEvent event) config gui = gui
update (PanelEvent event) config gui = gui
