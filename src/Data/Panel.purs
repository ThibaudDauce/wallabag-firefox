module Data.Panel where

import Control.Monad.Eff (Eff)
import Data.Config (Event(BaseUrlHasBeenChanged), BaseUrl(BaseUrl), createApiClientHasBeenChanged) as Config
import Prelude (Unit, ($), (<$>), (>>>), bind)
import Sdk.Panel (PANEL, Panel, create, onPanelShow, onPanelHide, onNewBaseUrl, onNewApiClient) as Sdk
import Data.Url (parse) as Url

data Type = NoBaseUrl | NoApiClient String
data Status = Hidden | Showing Type | AskedToShow Type
newtype Panel = Panel { status :: Status
                      , sdkPanel :: Sdk.Panel
                      }

data Event = PanelWasHidden | PanelWasShown

create :: forall eff. Eff (panel :: Sdk.PANEL | eff) Panel
create = Panel <$> { status: Hidden
                   , sdkPanel: _
                   } <$> Sdk.create

addEventListeners :: forall a b. Panel -> (Event -> Eff a Unit) -> (Config.Event -> Eff a Unit) -> Eff (panel :: Sdk.PANEL | b) Unit
addEventListeners (Panel panel) sendGuiEvent sendConfigEvent = do
  Sdk.onPanelShow panel.sdkPanel (sendGuiEvent PanelWasShown)
  Sdk.onPanelHide panel.sdkPanel (sendGuiEvent PanelWasHidden)
  Sdk.onNewBaseUrl panel.sdkPanel (Url.parse >>> Config.BaseUrl >>> Config.BaseUrlHasBeenChanged >>> sendConfigEvent)
  Sdk.onNewApiClient panel.sdkPanel (\clientId secretKey -> sendConfigEvent $ Config.createApiClientHasBeenChanged clientId secretKey)
