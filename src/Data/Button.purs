module Data.Button where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe(Nothing))
import Sdk.Ui.Button.Toggle (BUTTON, Button, create) as Sdk

data Event = ButtonWasClicked

create :: forall eff. (Eff (button :: Sdk.BUTTON | eff) Sdk.Button)
create = Sdk.create { id: "Wallabag"
                    , label: "Wallabag it!"
                    , icon: "./images/icon.svg"
                    , disabled: Nothing
                    , onChange: Nothing
                    , onClick: Nothing
                    , badge: Nothing
                    , badgeColor: Nothing }
