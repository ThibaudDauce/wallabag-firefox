module Data.State where

import Data.Config (Event, Config, Action) as Config
import Data.Entries (Entries, Event) as Entries
import Data.Gui (Event, Gui) as Gui
import Prelude ((<>), show, class Show)

newtype State = State { entries :: Entries.Entries
                      , config :: Config.Config
                      , gui :: Gui.Gui
                      }

instance showState :: Show State where
  show (State state) = "State of the application:\n"
                    <> "--- Entries ---\n" <> show state.entries <> "\n"
                    <> "--- Config ---\n" <> show state.config <> "\n"

data Event = EntriesEvent Entries.Event
           | ConfigEvent Config.Event
           | GuiEvent Gui.Event

instance showEvent :: Show Event where
  show (EntriesEvent event) = "New event concerning the entries: " <> show event
  show (ConfigEvent event) = "New event concerning the config: " <> show event
  show (GuiEvent event) = "New event concerning the GUI"

data Action = ConfigAction Config.Action

getGui :: State -> Gui.Gui
getGui (State state) = state.gui

getEntries :: State -> Entries.Entries
getEntries (State state) = state.entries
