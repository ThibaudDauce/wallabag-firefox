module Data.Remote where

import Data.Synchronisable (Synchronisable)
import Prelude (show, (<>), class Show)

data Remote a = Local (Synchronisable a) a | Remote (Synchronisable a) a

instance showRemote :: (Show a) => Show (Remote a) where
  show (Local sync x) = "Local info " <> show x <> ", " <> show sync
  show (Remote sync x) = "Remote info " <> show x <> ", " <> show sync
