module Url where

data Url = InvalidUrl | Unsaved String | Saved String | Saving String
