module Panel where

import Control.Monad.Eff (Eff)
import Prelude (Unit)
import Sdk.Panel (Panel, PANEL, show) as Sdk
import Sdk.Ui.Button.Toggle (Button)

data PanelHasBeenChanged = Hide | Shown
data Panel = NoBaseUrl | NoApiClient String

open :: forall eff. Button -> Sdk.Panel -> Panel -> (Eff (panel :: Sdk.PANEL | eff) Unit)
open button panel NoBaseUrl = Sdk.show panel { contentURL: "./panels/no_url.html", width: 350, height: 370, position: button}
open button panel (NoApiClient _) = Sdk.show panel { contentURL: "./panels/no_api_client.html", width: 600, height: 570, position: button}
