'use strict'

const _ = require('sdk/l10n').get

exports.translate = function (key) {
  return function () {
    const value = _(key)
    if (value === null || value === undefined || value === '') {
      return key
    } else {
      return value
    }
  }
}
