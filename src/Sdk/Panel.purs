module Sdk.Panel where

import Control.Monad.Eff (Eff)
import Prelude (Unit)
import Sdk.Ui.Button.Toggle (Button)

type Panel = { isShowing :: Boolean }
type PanelShowOptions = { contentURL :: String
                        , width :: Int
                        , height :: Int
                        , position :: Button
                        }

foreign import data PANEL :: !
foreign import create :: forall eff. (Eff (panel :: PANEL | eff) Panel)
foreign import onPanelHide :: forall a b. Panel -> (Eff a Unit) -> (Eff (panel :: PANEL | b) Unit)
foreign import onPanelShow :: forall a b. Panel -> (Eff a Unit) -> (Eff (panel :: PANEL | b) Unit)
foreign import show :: forall eff. Panel -> PanelShowOptions -> (Eff (panel :: PANEL | eff) Unit)
foreign import onNewBaseUrl :: forall a b. Panel -> (String -> Eff a Unit) -> (Eff (panel :: PANEL | b) Unit)
foreign import onNewApiClient :: forall a b. Panel -> (String -> String -> Eff a Unit) -> (Eff (panel :: PANEL | b) Unit)
