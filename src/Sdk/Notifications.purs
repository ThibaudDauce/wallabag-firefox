module Sdk.Notifications where

import Control.Monad.Eff (Eff)
import Prelude (Unit)

type Notification = { title :: String
                    , text :: String
                    , iconURL :: String
                    }

foreign import data NOTIFICATIONS :: !
foreign import notify :: forall eff. Notification -> (Eff (notifications :: NOTIFICATIONS | eff) Unit)
