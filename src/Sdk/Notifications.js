'use strict'

const notifications = require('sdk/notifications')

exports.notify = function (options) {
  return function () {
    notifications.notify(options)
  }
}
