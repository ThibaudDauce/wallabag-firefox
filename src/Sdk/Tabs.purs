module Sdk.Tabs where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe(Just, Nothing))
import Prelude (Unit, (<$>))

foreign import data TABS :: !
foreign import getCurrentTabUrl :: forall eff. Eff (tabs :: TABS | eff) String
foreign import onTabChange :: forall a b. (String -> Eff a Unit) -> (Eff (tabs :: TABS | b) Unit)

getCurrentUrl :: forall eff. Eff (tabs :: TABS | eff) (Maybe String)
getCurrentUrl = validate <$> getCurrentTabUrl
  where
  validate "" = Nothing
  validate url = Just url
