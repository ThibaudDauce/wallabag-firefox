'use strict'

const tabs = require('sdk/tabs')

exports.getCurrentTabUrl = function () {
  return tabs.activeTab.url
}

exports.onTabChange = function (callback) {
  return function () {
    tabs.on('activate', function () {
      callback(tabs.activeTab.url)()
    })
    tabs.on('ready', function () {
      callback(tabs.activeTab.url)()
    })
  }
}
