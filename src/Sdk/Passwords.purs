module Sdk.Passwords where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe(Just, Nothing))
import Prelude (Unit)

foreign import data PASSWORDS :: !
foreign import getImpl :: forall eff. Maybe String -> (String -> Maybe String) -> String -> (Maybe String -> (Eff (passwords :: PASSWORDS | eff) Unit)) -> (Eff (passwords :: PASSWORDS | eff) Unit)

get :: forall eff. String -> (Maybe String -> (Eff (passwords :: PASSWORDS | eff) Unit)) -> (Eff (passwords :: PASSWORDS | eff) Unit)
get realm = getImpl Nothing Just realm
