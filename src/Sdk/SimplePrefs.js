'use strict'

const sdkPrefs = require('sdk/simple-prefs')
const prefs = sdkPrefs.prefs

exports.getPreferenceImpl = function (key) {
  return function (nothing) {
    return function (just) {
      return function () {
        const value = prefs[key]
        if (value === null || value === undefined || value === '') {
          return nothing
        } else {
          return just(value)
        }
      }
    }
  }
}

exports.setPreference = function (key) {
  return function (value) {
    return function () {
      console.log('Setting new value ' + value + ' for key ' + key)
      prefs[key] = value
    }
  }
}

exports.onChange = function (key) {
  return function (callback) {
    return function () {
      console.log('Setting listener for ' + key)
      sdkPrefs.on(key, function () {
        console.log(key + ' was modified')
        callback(prefs[key])()
      })
    }
  }
}
