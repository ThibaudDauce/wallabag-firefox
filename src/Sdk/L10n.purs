module Sdk.L10n where

import Control.Monad (map)
import Control.Monad.Eff (Eff)
import Data.Maybe (fromMaybe, Maybe(Just, Nothing))
import Prelude (id)

foreign import data L10N :: !
foreign import translate :: forall eff. String -> (Eff (l10n :: L10N | eff) String)
