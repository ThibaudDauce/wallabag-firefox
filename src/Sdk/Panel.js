'use strict'

const panel = require('sdk/panel')

exports.create = function () {
  return panel.Panel({})
}

exports.onPanelHide = function (panel) {
  return function (callback) {
    return function () {
      panel.on('hide', callback)
    }
  }
}

exports.onPanelShow = function (panel) {
  return function (callback) {
    return function () {
      panel.on('show', callback)
    }
  }
}

exports.show = function (panel) {
  return function (options) {
    return function () {
      panel.contentURL = options.contentURL
      panel.show(options)
    }
  }
}

exports.onNewBaseUrl = onPanelPort('new-base-url')
exports.onNewApiClient = onPanelPort('on-new-api-client')

function onPanelPort (name) {
  return function (panel) {
    return function (callback) {
      return function () {
        panel.port.on(name, function () {
          const args = Array.from(arguments)
          for (var i = 0; i < args.length; i++) {
            callback = callback(args[i])
          }
          callback()
        })
      }
    }
  }
}
